/*
Write a recursive function pattern(n) which prints a number triangle.

For example pattern(4) should print the following pattern on the terminal

1

21

321

4321
*/

#include<stdio.h>

int main(){

    int n=0,i=1;

    printf("\nEnter a Number : ");
    scanf("%d", &n);

    numberTriangle(n);

    return 0;

}

int numberTriangle(int n){

    if(1<=n)
    {
        numberTriangle(n-1);

        while(n!=0)
        {
            printf("%d", n);
            n-=1;
        }
        printf("\n");
    }
    return n;
}
